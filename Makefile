#export FOLDED_POT_MATERIAL_WIDTH :=  
#export FOLDED_POT_MATERIAL_HEIGHT := 
export FOLDED_POT_SCALEXY :=         1
export FOLDED_POT_NUM_WIDTHS_WIDE := 3 # 3 or 5
export FOLDED_POT_LEFT_MARGIN :=     0 # in
export FOLDED_POT_BOTTOM_MARGIN :=   0 # in

all: core small

core-square := 8.5-folded-pot.ps
core-rect :=   8.5x11-folded-pot.ps

core:
	./folded-pot.pl 8.5    > ${core-square}
	./folded-pot.pl 8.5 11 > ${core-rect}

core :=\
  ${core-square}\
  ${core-rect}

small:
	./folded-pot.pl 7.5 10 > 7.5x10-folded-pot.ps
	./folded-pot.pl 7.5    > 7.5-folded-pot.ps

small :=\
	7.5x10-folded-pot.ps\
	7.5-folded-pot.ps

all :=\
	${core}\
	${small}

open: open-all
open-all: all
	./open.sh ${all}

open-core: core
	./open.sh ${core}

open-small: small
	./open.sh ${small}

clean:
	rm -f ${all}
