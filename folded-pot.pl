#!/usr/bin/perl -w
# -*- mode: cperl -*-
# -*- cperl-close-paren-offset: -2 -*-
# -*- cperl-continued-statement-offset: 2 -*-
# -*- cperl-indent-level: 2 -*-
# -*- cperl-indent-parens-as-block: t -*-
# -*- cperl-tab-always-indent: t -*-
# -*- tab-width: 2
# -*- indent-tabs-mode: nil

use strict;
use warnings;

my $paper_size_name = 'a4';

if ((exists $ENV{'LC_PAPER'} && $ENV{'LC_PAPER'} =~ /^en_US/) ||
    (exists $ENV{'LANG'}     && $ENV{'LANG'}     =~ /^en_US/)) {
  $paper_size_name = 'us-letter';
}
# US Letter: 8.5 x 11 in
# A4: 210 x 297 mm
my $paper_size = {
  'us-letter' => { 'width' => 612, 'height' => 792 }, # pts
  'a4' =>        { 'width' => 595, 'height' => 842 }, # pts
};
my $material_width =  $$paper_size{$paper_size_name}{'width'}/72;
my $material_height = $$paper_size{$paper_size_name}{'height'}/72;
$material_width =  $ENV{'FOLDED_POT_MATERIAL_WIDTH'}  if $ENV{'FOLDED_POT_MATERIAL_WIDTH'};
$material_height = $ENV{'FOLDED_POT_MATERIAL_HEIGHT'} if $ENV{'FOLDED_POT_MATERIAL_HEIGHT'};

my $num_widths_wide = 3; # 3 or 5
$num_widths_wide = $ENV{'FOLDED_POT_NUM_WIDTHS_WIDE'} if $ENV{'FOLDED_POT_NUM_WIDTHS_WIDE'};

my $scalexy = 1;
$scalexy = $ENV{'FOLDED_POT_SCALEXY'} if $ENV{'FOLDED_POT_SCALEXY'};

my $left_margin =   0;
my $bottom_margin = 0;
$left_margin =   $ENV{'FOLDED_POT_LEFT_MARGIN'}   if $ENV{'FOLDED_POT_LEFT_MARGIN'};
$bottom_margin = $ENV{'FOLDED_POT_BOTTOM_MARGIN'} if $ENV{'FOLDED_POT_BOTTOM_MARGIN'};

my $ws = '  ';
my $nl = "\n";
my $num_dimensions = scalar @ARGV;
my $dimension = {};
if (0) {
} elsif (2 == $num_dimensions) {
  $$dimension{'width'} =  $ARGV[0];
  $$dimension{'height'} = $ARGV[1];
} elsif (1 == $num_dimensions) {
  $$dimension{'width'} =  $ARGV[0];
  $$dimension{'height'} = $$dimension{'width'};
} elsif (0 == $num_dimensions) {
  $$dimension{'width'} =  $material_width;
  $$dimension{'height'} = $material_height;
  $num_dimensions = 2;
} else {
  die "$0: error: only 0, 1, or 2 dimensions are allowed." . $nl;
}
my ($width, $height);

die if $$dimension{'width'} + $left_margin > $material_width;
$width = $$dimension{'width'}/$num_widths_wide;

if (1 == $num_dimensions) {
  $height = $width;
} elsif (2 == $num_dimensions) {
  die if $$dimension{'width'} > $$dimension{'height'};
  die if $$dimension{'height'} + $bottom_margin > $material_height;
  $height = $$dimension{'height'} - (2 * $width);
}
my $echo_volume = 1;
$echo_volume = $ENV{'FOLDED_POT_ECHO_VOLUME'} if $ENV{'FOLDED_POT_ECHO_VOLUME'};

if ($echo_volume) {
  my $volume_in_mL = ($width * 2.54)**2 * $height * 2.54;
  printf STDERR "width/height of %.2f/%.2f cm or %.2f/%.2f in yields a volume of approx %3.0f mL or %02.1f oz" . $nl,
    $width * 2.54, $height * 2.54,
    $width,        $height,
    $volume_in_mL, $volume_in_mL/29.57;
}
# width = n + (n * sqrt(2))
# ...
# n = width/(1 + sqrt(2))
my $filestr =
  "%!PS-Adobe EPSF-3.0" . $nl .
  "%%BoundingBox: 0 0 $$paper_size{$paper_size_name}{'width'} $$paper_size{$paper_size_name}{'height'}" . $nl .
  "%%EOF" . $nl;

$filestr .=
  $nl .
  "/in { 72 mul } def" . $nl .
  "/left_margin  $left_margin in  def" . " % change me if you like" . $nl .
  "/bottom_margin $bottom_margin in def" . " % change me if you like" . $nl .
  "/width  $width in def" . " % change me if you like" . $nl .
  "/height $height in def" . " % change me if you like" . $nl .
  $nl .
  "/tile_width  width  1.5 mul def" . $nl .
  "/tile_height width height 0.5 mul add def" . $nl .
  "/n width 2 sqrt 1 add div def" . $nl .
  "/m 2 sqrt n mul 2 div def" . $nl .
  "/scalexy $scalexy def" . $nl .
  "/debug false def" . $nl .
  $nl;

if (5 == $num_widths_wide) {
  $filestr .=
    "/originx left_margin   width add def" . $nl .
    "/originy bottom_margin width add def" . " % yes, width, not height" . $nl;
} else {
  $filestr .=
    "/originx left_margin   def" . $nl .
    "/originy bottom_margin def" . $nl;
}
$filestr .=
  "/origin { originx originy } def" . $nl .
  "/black   { 0 0 0 } def" . $nl .
  "/lime    { 0 1 0 } def" . $nl .
  "/magenta { 1 0 1 } def" . $nl .
  "/red     { 1 0 0 } def" . $nl .
  "/linewidth 1 def" . $nl;

$filestr .=
  &fold_out_edges('lime',    'linewidth') . $nl .
  &fold_in_edges ('magenta', 'linewidth') . $nl .
  &material_edges('black',   'linewidth') . $nl .
  &debug_edges   ('red',     'linewidth') . $nl;

$filestr .=
  "/tile {" . $nl .
  $ws . "gsave" . $nl;
$filestr .=
  $nl .
  $ws . "fold_out_edges" . $nl .
  $ws . "fold_in_edges" . $nl .
  $ws . "material_edges" . $nl .
  $ws . "debug {" . $nl .
  $ws . $ws . "debug_edges" . $nl .
  $ws . "} if" . $nl;
$filestr .=
  $nl .
  $ws . "grestore" . $nl .
  "} def" . $nl;
##
$filestr .=
  "gsave" . $nl .
  "scalexy scalexy scale" . $nl;
$filestr .=
  $nl .
  "gsave" . $nl .
  "origin translate" . $nl .
  #
  "1 1 scale" . $nl .
  "tile" . $nl .
  "grestore" . $nl;

if (1) { # set to zero if working on the tile
  $filestr .=
    $nl .
    "gsave" . $nl .
    "origin translate" . $nl .
    "width 3 mul 0 translate" . $nl .
    "-1 1 scale" . $nl .
    "tile" . $nl .
    "grestore" . $nl;

  $filestr .=
    $nl .
    "gsave" . $nl .
    "origin translate" . $nl .
    "width 3 mul width 2 mul height add translate" . $nl .
    "-1 -1 scale" . $nl .
    "tile" . $nl .
    "grestore" . $nl;

  $filestr .=
    $nl .
    "gsave" . $nl .
    "origin translate" . $nl .
    "0 width 2 mul height add translate" . $nl .
    "1 -1 scale" . $nl .
    "tile" . $nl .
    "grestore" . $nl;
}
##
$filestr .=
  $nl .
  "showpage" . $nl .
  "grestore" . $nl;

print $filestr;

sub fold_out_edges {
  my ($rgbcolor, $linewidth) = @_;
  my $result =
    "/fold_out_edges {" . $nl;
  $result .=
    $ws . "$rgbcolor setrgbcolor" . $nl .
    $ws . "$linewidth setlinewidth" . $nl .
    $ws . "newpath" . $nl;
  $result .=
    $nl . ##
    $ws . "0 0 moveto" . $nl .
    $ws . "45 neg rotate" . $nl .
    $ws . "0 2 sqrt m mul rlineto" . $nl .
    $ws . "45 rotate" . $nl .
    $nl . ##
    $ws . "0 0 moveto" . $nl .
    $ws . "0 n 2 sqrt mul rmoveto" . $nl .
    $ws . "width n rlineto" . $nl .
    $ws . "n neg width neg rlineto" . $nl .
    $ws . "closepath" . $nl;
  $result .=
    $nl .
    $ws . "stroke" . $nl .
    "} def";
  return $result;
}
sub fold_in_edges {
  my ($rgbcolor, $linewidth) = @_;
  my $result =
    "/fold_in_edges {" . $nl;
  $result .=
    $ws . "$rgbcolor setrgbcolor" . $nl .
    $ws . "$linewidth setlinewidth" . $nl .
    $ws . "newpath" . $nl;
  $result .=
    $nl . ##
    $ws . "0 0 moveto" . $nl .
    $ws . "m m rmoveto" . $nl . # 45 degrees (move)
    $ws . "45 neg rotate" . $nl .
    $ws . "0 2 sqrt width mul 2 sqrt m mul sub rlineto" . $nl . # line bisecting the triangle
    $ws . "45 rotate" . $nl;
  $result .=
    $nl . ##
    $ws . "0 0 moveto" . $nl .
    $ws . "width 0 rmoveto" . $nl .
    $ws . "0 height 0.5 mul width add rlineto" . $nl; # right vertical line
  $result .=
    $nl . ##
    $ws . "0 0 moveto" . $nl .
    $ws . "0 width rmoveto" . $nl .
    $ws . "width 1.5 mul 0 rlineto" . $nl; # top horizontal line
  $result .=
    $nl .
    $ws . "stroke" . $nl .
    "} def";
  return $result;
}
sub material_edges {
  my ($rgbcolor, $linewidth) = @_;
  my $result =
    "/material_edges {" . $nl;
  $result .=
    $ws . "$rgbcolor setrgbcolor" . $nl .
    $ws . "$linewidth setlinewidth" . $nl .
    $ws . "newpath" . $nl;
  $result .=
    $nl . ##
    $ws . "0 0 moveto" . $nl .
    $ws . "0 tile_height rmoveto" . $nl .
    $ws . "0 tile_height neg rlineto" . $nl .
    $ws . "tile_width 0 rlineto" . $nl;
  $result .=
    $nl .
    $ws . "stroke" . $nl .
    "} def";
  return $result;
}
sub debug_edges {
  my ($rgbcolor, $linewidth) = @_;
  my $result =
    "/debug_edges {" . $nl;
  $result .=
    $ws . "$rgbcolor setrgbcolor" . $nl .
    $ws . "$linewidth setlinewidth" . $nl .
    $ws . "newpath" . $nl .
    $nl . ##
    $ws . "0 0 moveto" . $nl .
    $ws . "0 height width sub rmoveto" . $nl .
    $ws . "45 neg rotate" . $nl .
    $ws . "0 2 sqrt width mul 1.5 mul rlineto" . $nl .
    $ws . "45 rotate" . $nl;
  $result .=
    $nl .
    $ws . "stroke" . $nl .
    "} def";
  return $result;
}
