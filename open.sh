#!/bin/bash
set -o errexit -o nounset -o pipefail
IFS=$'\n\t'

os_names[0]="linux";          os_names[1]="darwin"

os_regexs[0]=".*(L|l)inux.*"; os_regexs[1]=".*(D|d)arwin.*"
os_opens[0]="xdg-open";       os_opens[1]="open"

at() {
  keys=("${!1}")
  vals=("${!2}")
  key=$3
  for (( i=0; i < ${#keys[@]}; i++ )); do
    if [[ $key == "${keys[$i]}" ]]; then
      echo "${vals[$i]}"
      return 0
    fi
  done
  echo "$0: error: could not find value for key '$key'" 1>&2
  return 1
}
os_type() {
  names=("${!1}")
  regexs=("${!2}")

  # os_type: first try env var OSTYPE
  for name in ${names[@]}; do
    regex=$(at names[@] regexs[@] $name)
    if [[ -n "${OSTYPE-}" && $OSTYPE =~ $regex ]]; then
      echo $name
      return 0
    fi
  done
  # os_type: second try uname cmd
  if [[ $(type uname) ]]; then
    name=$(uname -s)
    regex=$(at names[@] regexs[@] $name)
    if [[ $name =~ $regex ]]; then
      echo $name
      return 0
    fi
  fi
  echo "$0: error: could not determine os-type" 1>&2
  return 1
}
os_type=$(os_type os_names[@] os_regexs[@])

open_cmd=$(at os_names[@] os_opens[@] $os_type)

$open_cmd $@
