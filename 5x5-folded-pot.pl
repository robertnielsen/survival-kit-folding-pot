#!/usr/bin/perl -w
# -*- mode: cperl -*-
# -*- cperl-close-paren-offset: -2 -*-
# -*- cperl-continued-statement-offset: 2 -*-
# -*- cperl-indent-level: 2 -*-
# -*- cperl-indent-parens-as-block: t -*-
# -*- cperl-tab-always-indent: t -*-
# -*- tab-width: 2
# -*- indent-tabs-mode: nil

use strict;

my $paper_size_name = 'us-letter';
my $num_lengths_wide = 5;

# US Letter: 8.5 x 11 in
# A4: 210 x 297 mm
my $paper_size = {
  'us-letter' => { 'width' => 612, 'height' => 792 }, # pts
  'a4' =>        { 'width' => 595, 'height' => 842 }, # pts
};
my $ws = '  ';
my $nl = "\n";

my $length = ($$paper_size{$paper_size_name}{'width'}/72 - 1/8 - 1/8)/$num_lengths_wide;
if (1) {
  my ($width, $height) = ($length, $length);
  my $volume_in_mL = ($width * 2.54)**2 * $height * 2.54;
  printf STDERR "width/height of %.2f/%.2f cm or %.2f/%.2f in yields a volume of approx %3.0f mL or %02.1f oz" . $nl,
    $width * 2.54, $height * 2.54,
    $width,        $height,
    $volume_in_mL, $volume_in_mL/29.57;
}

my $filestr =
  "%!PS-Adobe EPSF-3.0" . $nl .
  "%%BoundingBox: 0 0 $$paper_size{$paper_size_name}{'width'} $$paper_size{$paper_size_name}{'height'}" . $nl .
  "%%EOF" . $nl;
$filestr .=
  $nl .
  "/in { 72 mul } def" . $nl .
  "/length { $length in } def" . $nl .
  "/originx { 1 8 div in } def" . $nl .
  "/originy { 1 4 div in length add } def" . $nl .
  "/origin { originx originy } def" . $nl .

  $nl .
  "/tile {" . $nl .
  $ws . "newpath" . $nl .
  $ws . "0 0 moveto" . $nl .
  $ws . "length 2 mul 0 rmoveto" . $nl .

  $ws . "length neg 0 rlineto" . $nl .
  $ws . "0 length rlineto" . $nl .

  $ws . "0 0 rmoveto" . $nl .

  $ws . "length neg length rlineto" . $nl .
  $ws . "length 0 rlineto" . $nl .
  $ws . "closepath" . $nl .

  $ws . "length 0 rlineto" . $nl .
  $ws . "length neg length neg rlineto" . $nl .

  $ws . "length length  rmoveto" . $nl .

  $ws . "0 length rlineto" . $nl .
  $ws . "length neg 0 rlineto" . $nl .

  $ws . "length 2 div neg length 2 div neg rlineto" . $nl .

  $ws . "1 setlinewidth" . $nl .
  $ws . "stroke" . $nl .
  "} def" . $nl;
##
if (1) {
  $filestr .=
    $nl .
    "gsave" . $nl .
    "originx originy translate" . $nl .
    "tile" . $nl .
    "grestore" . $nl;
}
if (1) {
  $filestr .=
    $nl .
    "gsave" . $nl .
    "originx originy translate" . $nl .
    "length 4 mul length neg translate" . $nl .
    "90 rotate" . $nl .
    "tile" . $nl .
    "grestore" . $nl;

  $filestr .=
    $nl .
    "gsave" . $nl .
    "originx originy translate" . $nl .
    "length 5 mul length 3 mul translate" . $nl .
    "180 rotate" . $nl .
    "tile" . $nl .
    "grestore" . $nl;

  $filestr .=
    $nl .
    "gsave" . $nl .
    "originx originy translate" . $nl .
    "length length 4 mul translate" . $nl .
    "270 rotate" . $nl .
    "tile" . $nl .
    "grestore" . $nl;
}
##
$filestr .=
  $nl .
  "showpage" . $nl;

print $filestr;
